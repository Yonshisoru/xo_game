import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {
    Board board;
    Player x;
    Player o;

    Game(){
        x = new Player('x');
        o = new Player('o');
        board = new Board(x,o);
    }
    public void start() {
        do{
            printWelcome();
            showScore();
            try {
                do {
                    showTable();
                    showTurn();
                    input();
                } while (!board.isFinish());
                    showWinner();
            }catch(NullPointerException e){
                printDraw();
            }catch(Exception e) {
                printError();
                e.printStackTrace();
            }
        }while(checkReplay());
        showScore();
        printEnd();
    }

    private void showWinner(){
        if(board.getWinnerPlayer().getName()=='-'){
            printDraw();
        }else {
            System.out.println();
            printHashBar();
            System.out.println("Winner is " + board.getWinnerPlayer().getName());
            printHashBar();
            System.out.println();
        }
    }

    private void printDraw(){
        System.out.println();
        printHashBar();
        System.out.println("!!!!!!!!!!!!DRAW!!!!!!!!!!!!!!!");
        printHashBar();
        System.out.println();
    }
    private void printEnd(){
        System.out.println("Thank you for playing...");
    }
    private void printHashBar(){
        System.out.println("############################");
    }
    private boolean checkReplay(){
        while(true){
            Scanner scan = new Scanner(System.in);
            System.out.print("Did you want to rematch (y/n) :");
            char input = scan.next().charAt(0);
            if(input=='y') {
                board.clearBoard();
                return true;
            }else if(input=='n'){
                return false;
            }else{
                printError();
            }
        }

    }
    private void showScore(){
        //System.out.println("         Score          ");
        try {
            printBar();
            Thread.sleep(300);
            System.out.println("Player: " + x.getName());
            System.out.println("Win: " + x.getWin());
            System.out.println("Draw: " + x.getDraw());
            System.out.println("Lose: " + x.getLose());
            Thread.sleep(300);
            printBar();
            System.out.println("Player: " + o.getName());
            System.out.println("Win: " + o.getWin());
            System.out.println("Draw: " + o.getDraw());
            System.out.println("Lose: " + o.getLose());
            Thread.sleep(300);
            printBar();
        }catch(Exception e){

        }
    }
    private void printWelcome() {
        try {
            System.out.println("Welcome to XO Game :)");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void printBar() {
        System.out.println("------------------------------------------");
    }

    private void showTurn(){
        try {
            Thread.sleep(500);
            System.out.print("Turn ");
            System.out.print(board.getCurrentPlayer().getName());
            System.out.print(" .");
            System.out.print(".");
            System.out.print(".\n");
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void showTable(){
        try {
            Thread.sleep(500);
            for (int i = 0; i <= board.getTable().length; i++) {
                if (i != 0) {
                    System.err.print((i) + " ");
                    Thread.sleep(350);
                    for (int o = 0; o < board.getTable().length; o++) {
                        System.out.print(board.getTable()[i - 1][o] + " ");
                        Thread.sleep(200);
                    }
                    System.out.println();
                } else {
                    System.out.print("  ");
                }
                for (int k = 1; k <= board.getTable()[0].length; k++) {
                    if (i == 0) {
                        System.out.print((k) + " ");
                    }
                }
                if (i == 0) {
                    System.out.println();
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private void input() throws Exception {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.print("Input Your Selected Area (Row Column): ");
            int row = scan.nextInt();
            int col = scan.nextInt();
            if((row<=0||row>3)&&(col<=0||col>3)){
                printRangeError();
            }else {
                if (board.getTable()[row-1][col-1] == '*') {
                    board.setBoard(row, col);
                    showInput(row, col);
                    printBar();
                    board.switchTurn();
                } else {
                    printDuplicate();
                }
            }
        }catch(InputMismatchException e) {
            throw new InputMismatchException();
        }catch(NumberFormatException e){
            throw new NumberFormatException();
        }catch(Exception e){
            throw new Exception(e);
        }
    }

    private void printError(){
        System.err.println("Input Invalid\nTry Again!!");
    }
    private void printRangeError(){
        System.err.println("Input Invalid (Range muse be 1-3 only)\nTry Again!!");
    }
    private void printDuplicate(){
        System.err.println("This box is selected\nTry Again!!");
    }
    private void showInput(int row,int col){
        System.out.print("You choose ");
        System.out.print("Row: "+row+" Column: "+col+"\n");
    }

}
