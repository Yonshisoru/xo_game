public class Board {
    private char[][] table = new char[][]{{'*','*','*'},
            {'*','*','*'},
            {'*','*','*'}};
    private Player x;
    private Player o;
    private Player winner;
    private int turncount = 9;
    private Player current;

    Board(Player x,Player o){
        this.x = x;
        this.o = o;
        winner = new Player('-');
        randomPlayer(x, o);
    }

    public void randomPlayer(Player x, Player o) {
        double change = Math.random()*100;
        if(change>=50) {
            current = x;
        }else{
            current = o;
        }
    }

    public boolean isFinish(){
            /*
             *           |*--|
             *           |-*-|
             *           |--*|
             * */
            if(table[0][0]=='x'&&table[1][1]=='x'&&table[2][2]=='x'){
                winner = x;
                x.increaseWin();
                o.increaseLose();
                return true;
            }else if(table[0][0]=='o'&&table[1][1]=='o'&&table[2][2]=='o'){
                winner = o;
                o.increaseWin();
                x.increaseLose();
                return true;
            }
            /*
             *           |--*|
             *           |-*-|
             *           |*--|
             * */
            if(table[0][2]=='x'&&table[1][1]=='x'&&table[2][0]=='x'){
                 winner = x;
                x.increaseWin();
                o.increaseLose();
                return true;
            }else if(table[0][2]=='o'&&table[1][1]=='o'&&table[2][0]=='o'){
                winner = o;
                o.increaseWin();
                x.increaseLose();
                return true;
            }
            /*
             *           |***|
             *           |---|
             *           |---|
             * */
            if(table[0][0]=='x'&&table[0][1]=='x'&&table[0][2]=='x'){
                 winner = x;
                x.increaseWin();
                o.increaseLose();
                return true;
            }else if(table[0][0]=='o'&&table[0][1]=='o'&&table[0][2]=='o'){
                winner = o;
                o.increaseWin();
                x.increaseLose();
                return true;
            }
            /*
             *           |---|
             *           |***|
             *           |---|
             * */
            if(table[1][0]=='x'&&table[1][1]=='x'&&table[1][2]=='x'){
                winner = x;
                x.increaseWin();
                o.increaseLose();
                return true;
            }else if(table[1][0]=='o'&&table[1][1]=='o'&&table[1][2]=='o'){
                winner = o;
                o.increaseWin();
                x.increaseLose();
                return true;
            }
            /*
             *           |---|
             *           |---|
             *           |***|
             * */
            if(table[2][0]=='x'&&table[2][1]=='x'&&table[2][2]=='x'){
                 winner = x;
                x.increaseWin();
                o.increaseLose();
                return true;
            }else if(table[2][0]=='o'&&table[2][1]=='o'&&table[2][2]=='o'){
                winner = o;
                o.increaseWin();
                x.increaseLose();
                return true;
            }
            /*
             *           |*--|
             *           |*--|
             *           |*--|
             * */
            if(table[0][0]=='x'&&table[1][0]=='x'&&table[2][0]=='x'){
                winner = x;
                x.increaseWin();
                o.increaseLose();
                return true;
            }else if(table[0][0]=='o'&&table[1][0]=='o'&&table[2][0]=='o'){
                winner = o;
                o.increaseWin();
                x.increaseLose();
                return true;
            }
            /*
             *           |-*-|
             *           |-*-|
             *           |-*-|
             * */
            if(table[0][1]=='x'&&table[1][1]=='x'&&table[2][1]=='x'){
                 winner = x;
                x.increaseWin();
                o.increaseLose();
                return true;
            }else if(table[0][1]=='o'&&table[1][1]=='o'&&table[2][1]=='o'){
                winner = o;
                o.increaseWin();
                x.increaseLose();
                return true;
            }
            /*
             *           |--*|
             *           |--*|
             *           |--*|
             * */
            if(table[0][2]=='x'&&table[1][2]=='x'&&table[2][2]=='x'){
                winner = x;
                x.increaseWin();
                o.increaseLose();
                return true;
            }else if(table[0][2]=='o'&&table[1][2]=='o'&&table[2][2]=='o'){
                winner = o;
                o.increaseWin();
                x.increaseLose();
                return true;
            }

            if(turncount==0&&(winner.getName()!='x'||winner.getName()!='o')){
                o.increaseDraw();
                x.increaseDraw();
                return true;
            }else{
                return false;
            }
        }

    public char[][] getTable(){
        return this.table;
    }

    public Player getCurrentPlayer(){
        return current;
    }

    public Player getWinnerPlayer(){
        return winner;
    }

    public void setBoard(int row,int col){
        table[row-1][col-1] = current.getName();
    }

    public void clearBoard(){
        table = new char[][]{{'*','*','*'},
                {'*','*','*'},
                {'*','*','*'}};
        winner = new Player('-');
        randomPlayer(x, o);
        turncount = 9;
    }

    public void switchTurn(){
        if(current == o){
            current = x;
        }else{
            current = o;
        }
        turncount--;
    }
}
