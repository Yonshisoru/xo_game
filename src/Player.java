public class Player {
    private char name;
    private int win;
    private int draw;
    private int lose;

    Player(char name){
        this.name = name;
    }

    public char getName() {
        return name;
    }


    public int getWin() {
        return win;
    }

    public int getDraw() {
        return draw;
    }

    public int getLose() {
        return lose;
    }

    public void increaseWin() {
        this.win++;
    }

    public void increaseDraw() {
        this.draw++;
    }

    public void increaseLose() {
        this.lose++;
    }
}
